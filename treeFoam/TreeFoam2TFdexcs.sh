#!/bin/bash

chmod 666 /opt/TreeFoam/TreeFoamVersion

### step1 /opt/TreeFoam/treefoam ###
cp /opt/TreeFoam/treefoam treefoam.orig

sed -i -e "/pyFoamDir/s/~\/OpenFOAM\/PyFoam/\/usr\/local # for DEXCS/" /opt/TreeFoam/treefoam
N=`grep '#PATHの設定' -n /opt/TreeFoam/treefoam | cut -d ":" -f 1`
line="export dexcsSwakPath=/opt/DEXCS/SWAK #DEXCS"
sed -i -e "$N a $line" /opt/TreeFoam/treefoam
sed -i -e "/PYTHONPATH=/s/:PYTHONPATH/:\$dexcsSwakPath:PYTHONPATH/" /opt/TreeFoam/treefoam


### step2 /opt/TreeFoam/treefoam.py ###
cp /opt/TreeFoam/treefoam.py treefoam.py.orig

sed -i -e "/^version/s/\"$/+dexcsSwak\"/" /opt/TreeFoam/treefoam.py

sed -e "s/^/++/g" treefoam.import1.py > treefoam.import1.py.patch
N=`grep '#glade(ui)ファイル内を翻訳' -n /opt/TreeFoam/treefoam.py | cut -d ":" -f 1`
cat ./treefoam.import1.py.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.py
  N=$((N+1))
done

sed -e "s/^/++/g" treefoam.import2.py > treefoam.import2.py.patch
N=`grep '#menuBarの非表示設定' -n /opt/TreeFoam/treefoam.py | cut -d ":" -f 1`
cat ./treefoam.import2.py.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.py
  N=$((N+1))
done

sed -e "s/^/++/g" treefoam.import3.py > treefoam.import3.py.patch
N=`grep '#----- toolBar -------------------------' -n /opt/TreeFoam/treefoam.py | cut -d ":" -f 1`
cat ./treefoam.import3.py.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.py
  N=$((N+1))
done

sed -e "s/^/++/g" treefoam.import4.py > treefoam.import4.py.patch
N=`grep '#  ---------- copy paste 関連 --' -n /opt/TreeFoam/treefoam.py | cut -d ":" -f 1`
cat ./treefoam.import4.py.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.py
  N=$((N+1))
done

sed -i -e "s/^++//g" /opt/TreeFoam/treefoam.py


### step3 /opt/TreeFoam/treefoam.glade ###
cp /opt/TreeFoam/treefoam.glade treefoam.glade.orig

sed -e "s/^/++/g" treefoam.import1.glade > treefoam.import1.glade.patch
N=`grep '<object class="GtkApplicationWindow" id="window1">' -n /opt/TreeFoam/treefoam.glade | cut -d ":" -f 1`

cat ./treefoam.import1.glade.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.glade
  N=$((N+1))
done

sed -e "s/^/++/g" treefoam.import2.glade > treefoam.import2.glade.patch
N0=`grep '<property name="label" translatable="yes">ヘルプ(_H)</property>' -n /opt/TreeFoam/treefoam.glade | cut -d ":" -f 1`

N=$((N0-4))

cat ./treefoam.import2.glade.patch | while read line
do
  echo "$N'i' $line"
  sed -i -e "$N i $line" /opt/TreeFoam/treefoam.glade
  N=$((N+1))
done

sed -i -e "s/^++//g" /opt/TreeFoam/treefoam.glade

