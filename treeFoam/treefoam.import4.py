    #----Dexcs用追加--------------
    def dexcsReload(self):
        self.writeStatusBar(u"tree data を読み込み中...")
        #currentCaseを保存
        item = self.treeView.get_selection()
        #selCaseDir = self.getDirectory(item)
        #再読込
        self.reload()
        #caseName, solverをlabelにセット
        #caseItem = self.getItem(solveCaseDir)
        #self.changeSolveCase(caseItem)
        #currentCaseを設定
        #self.setSelectCase(self.convDirToList(selCaseDir))
        #self.clearStatus()
    def runDexcsDispBlockMesh(self,event):
        #global solveCaseDir, envOpenFOAMFix
        pyDexcsSwak.dispBlockMesh(solveCaseDir,envOpenFOAMFix)
        #event.Skip()
    def runDexcsBlockMesh(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runBlockMesh(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsCheckMesh(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runCheckMesh(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsCfMesh(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runCfMesh(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsAllrun(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runAllrun(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsAllclean(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runAllclean(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsSubmitJob(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runSubmitJob(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsSubmitStatus(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runSubmitStatus(solveCaseDir, envOpenFOAMFix)
        self.dexcsReload()
        #event.Skip()
    def runDexcsJGP(self,event):
        #global solveCaseFix, envOpenFOAMFix
        pyDexcsSwak.runJGP(solveCaseDir, envOpenFOAMFix)
        #event.Skip()
    def runDexcsKdiff3(self,event):
        os.system("kdiff3 &")
        #event.Skip()
    def runDexcsSearch(self,event):
        #global envOpenFOAMFix
        pyDexcsSwak.runSearch(envOpenFOAMFix)
        #event.Skip()
    #-----------------------------

