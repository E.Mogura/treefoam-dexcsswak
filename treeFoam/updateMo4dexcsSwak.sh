#!/bin/bash

### step4 /opt/TreeFoam/data/locale/inputFiles ###
cat inputFiles.append >> /opt/TreeFoam/data/locale/inputFiles
### step5 /opt/TreeFoam/data/locale/treefoam.po ###
cat treefoam.po.append >> /opt/TreeFoam/data/locale/treefoam.po

### step6 /opt/TreeFoam/data/locale/treefoam.mo ###
cd /opt/TreeFoam/data/locale
./makeMoFile
cp treefoam.mo ./en_US/LC_MESSAGES/
