# これは何か
- TreeFoanを十徳ナイフ付き版に変更する為のマテリアル一式。
- TreeFoam-2系では、十徳ナイフ付き版も同時に公開されていたが、TreeFoam-3系では公開されていないので、十徳ナイフ付き版への変更方法・内容を公開する事にした。
- DEXCS（DEXCS for OpenFOAM で構築した計算環境）で無い場合にも使えるように必要モジュールを同梱した。

# 使用方法
- 同梱のスクリプト（TF2TF+dexcsSwak.sh）を管理者権限で実行すれば良い。
`sudo ./TF2TF+dexcsSwak.sh`
- 念の為、バックアップをとっておいた方が良いかもしれない。
`sodo cp -r /opt/TreeFoam /opt/TreeFoam.backup`

# 動作環境
- /opt/TreeFoam に収納されたTreeFoam-3系が稼働している事。
- DEXCSで無い環境の場合には、/opt/DEXCS/SWAKというフォルダを作成しておく事。
