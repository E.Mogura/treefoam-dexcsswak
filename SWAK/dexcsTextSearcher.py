# -*- coding:utf-8 -*-
import sys
import os
import shlex
import subprocess
import gi
import gettext
import treeview
import locale
localeDir = os.getenv("TreeFoamPath") + "/data/locale"
locale.bindtextdomain("treefoam", localeDir)

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class GtkGladeSample:
    
    def __init__(self):

        #global builder, window

        self.instDirOF = os.path.expanduser("~") + "/OpenFOAM/OpenFOAM-v1906"
        searchRootOrg = self.instDirOF
        self.instDirThis = os.path.abspath(os.path.dirname(__file__))
        if os.path.exists(self.instDirThis + "/search"):
            self.searchCmd = self.instDirThis + "/search"
        else:
            print(_("search コマンドが存在しません"))


        self.builder = Gtk.Builder()
        self.builder.set_translation_domain("treefoam")
        #sample.gladeファイルから画面を作成
        self.builder.add_from_file(os.path.dirname(os.path.abspath(__file__)) + os.sep + "dexcsTextSearcher.glade")
        #windows1というオブジェクトを取得
        self.window = self.builder.get_object("window1")
        #sample.gladeで定義されているシグナルと連結
        self.builder.connect_signals(self)

        self.window.connect("delete-event", self.closeMeSoon)
        #self.GtkApplicationWindow.text_1.Text = searchRoot

        #searchRoot = self.builder.get_object("text_1")
        #self.getParam(searchRoot, searchText,searchOpt)
        self.getParam()
        self.searchRoot.set_text(searchRootOrg) 

    def getParam(self):
        self.searchRoot = self.builder.get_object("text_1")
        #print(self.searchRoot.get_text())
        self.searchText = self.builder.get_object("text_2")
        #print(self.searchText.get_text())

        self.fileExtension = ""

        self.searchOpt1 = self.builder.get_object("check_1")
        if self.searchOpt1.get_active():
        	#print(self.searchOpt1.get_label())
        	self.fileExtension += self.searchOpt1.get_label()
        self.searchOpt2 = self.builder.get_object("check_2")
        if self.searchOpt2.get_active():
        	#print(self.searchOpt2.get_label())
        	self.fileExtension += self.searchOpt2.get_label()
        self.searchOpt3 = self.builder.get_object("text_3")
        #print(self.searchOpt3.get_text())
        self.fileExtension += self.searchOpt3.get_text()

    def main(self):
        #GUIを表示
        self.window.show()
        #Gtkのloop
        Gtk.main()

    def closeMeSoon(self, window, event):
        self.window.destroy()

    def closeMe(self, event):
        message = _("dexcsTextSearcherを終了しますか？")
        dialog = Gtk.MessageDialog(parent=self.window,
            flags=0,
            type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            message_format=_("終了確認")
        )
        dialog.format_secondary_text(
            message
        )
        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.YES:
        	self.window.destroy()

 

    def selectRoot(self, button_1):
        """
        検索場所ボタンを押した時の処理。ダイアログを表示する
        """
        fn = Gtk.FileChooserDialog(
                _("検索場所を選択してください"),
                self.window,
                Gtk.FileChooserAction.SELECT_FOLDER,
                (
                    Gtk.STOCK_CANCEL,
                    Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN,
                    Gtk.ResponseType.OK,
                ),
            )
        fn.show()
        resp = fn.run()
        if resp == Gtk.ResponseType.OK:
            fileName = fn.get_filename()
            print(fileName)
        fn.destroy() 
        if os.path.isdir(fileName):

        	self.searchRoot.set_text(fileName) 


        
    def runSearch(self, button_2):
        """
        検索開始ボタンを押した時の処理。ダイアログを表示する
        """
        
        self.getParam()
        #print(self.searchRoot.get_text())
        #print(self.searchText.get_text())
        #print(self.fileExtension)


        if os.path.exists(self.searchRoot.get_text()):
            self.rootFolder=self.searchRoot.get_text()
        else:
            self.rootFolder=self.instDirOF
        os.chdir(self.rootFolder)
        
        extension = self.fileExtension
        
        if self.searchText.get_text() != "":
            cmd = self.searchCmd + " " + extension + " " +  self.searchText.get_text()
            p = subprocess.run(shlex.split(cmd), encoding='utf-8', stdout=subprocess.PIPE)

            result=p.stdout.splitlines()

            ir = 0
            resultX=[]
            for line in result:
                print( ir, line)
                ir = ir+1
                resultX.append(line.rstrip("\n"))

            treeview.TreeView(resultX)


#    def main(self):
#        Gtk.main()
if __name__ == "__main__":
    app = GtkGladeSample()
    app.main()

