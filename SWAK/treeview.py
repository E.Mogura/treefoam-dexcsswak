# -*- coding:utf-8 -*-
import sys
import os
import shlex
import subprocess
import gi
import gettext
import locale
localeDir = os.getenv("TreeFoamPath") + "/data/locale"
locale.bindtextdomain("treefoam", localeDir)

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class TreeView:
    
    def __init__(self, element_array):

        self.builder = Gtk.Builder()
        self.builder.set_translation_domain("treefoam")
        #treeview_example_window.gladeファイルから画面を作成
        self.builder.add_from_file(os.path.dirname(os.path.abspath(__file__)) + os.sep + "treeviewScroll.glade")
        #treeview_example_windowというオブジェクトを取得
        self.window = self.builder.get_object("treeview_window")
        #treeview_example_window.gladeで定義されているシグナルと連結
        self.builder.connect_signals(self)
          #画面を閉じる（ｘ）を押した時の処理
        self.window.connect("delete-event", self.closeMeSoon)

        #ListStoreオブジェクトを取得
        self.ListStore = self.builder.get_object("liststore1")

        #ListStoreオブジェクトにリストを追加
        for i in range(len(element_array)):
        	treeiter = self.ListStore.append(['filelist'])
        	self.ListStore[treeiter][0]=element_array[i]

        self.window.show()
        #Gtkのloop
        Gtk.main()

    def main(self):
        #GUIを表示
        self.window.show()
        #Gtkのloop
        Gtk.main()

    def activate_list(self, *args):
        row=args[1]
        path = Gtk.TreePath(row)
        treeiter = self.ListStore.get_iter(path)
        data = self.ListStore.get_value(treeiter, 0)
        #print("activate",data)
        file_name = data.split(':')[0]
        if os.path.exists(file_name):
            os.system("gedit " + file_name + "&")

    def closeMeSoon(self, window, event):
        self.window.destroy()

if __name__ == "__main__":
    element_array = ("element_1","element_2","element_4","element_3","element_5")
    app = TreeView()
    app.main()

